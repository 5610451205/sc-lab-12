package part2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class AverageCalculate {
	private String readFile;
	private String readFile2;
	private String writeFile;
	private FileReader filereader;
	private FileWriter filewriter;
	
	public AverageCalculate(String readFile, String readFile2, String writeFile){
		this.readFile = readFile;
		this.readFile2 = readFile2;
		this.writeFile = writeFile;
	}
	
	public void calculateAverage(){
		double average = 0;
		try{
			//System.out.println("--------- Homework Scores ---------");
			//System.out.println("Name   Average");
			//System.out.println("=====  =========");
			filereader = new FileReader(this.readFile);
			BufferedReader buffer = new BufferedReader(filereader);
			
			filewriter = new FileWriter(writeFile, true);
			PrintWriter output = new PrintWriter(new BufferedWriter(filewriter));
			
			output.println("--------- Homework Scores ---------");
			output.println("Name   Average");
			output.println("=====  =========");
			
			for(String line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] s = line.split(", ");
				for(int i = 1; i < s.length; i++){
					average += Double.parseDouble(s[i]);
				}
				average = average / (s.length - 1);
				output.println(s[0] + "Average : " + average);
				//System.out.println(s[0] + "Average : " + average);
				average = 0;
			}
			output.println("===============================");
			output.flush();
			
			filereader = new FileReader(this.readFile2);
			BufferedReader buffer2 = new BufferedReader(filereader);
			
			output.println("--------- Exam Scores ---------");
			output.println("Name Average");
			output.println("==== =======");
			
			for(String line = buffer2.readLine(); line != null; line = buffer2.readLine()){
				String[] s = line.split(", ");
				for(int i = 1; i < s.length; i++){
					average += Double.parseDouble(s[i]);
				}
				average = average / (s.length - 1);
				output.println(s[0] + "Average Exam :  "+ average);
				output.flush();
				average = 0;
			}
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+ readFile);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file" + readFile);
		}
		finally{
			try {
				if (filereader != null)
					filereader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
}
