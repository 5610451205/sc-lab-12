package part4;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;


public class WordCounter {
	HashMap<String, Integer> wordCount ;
	private String massage;
	FileReader filereader ;
	
	public WordCounter(String massage){
		this.massage = massage;
		wordCount = new HashMap<String, Integer>();
		
	}
	
	public void count(){
		int value = 1;
		try{
			filereader = new FileReader(massage);
			BufferedReader buffer = new BufferedReader(filereader);
			
			String[] word = null;
			//for(int i = 0; i < s.length; i++){
				//wordCount.put(s[i], value);
			//}
			for(String line = buffer.readLine(); line != null; line = buffer.readLine()){
				
				word = line.split(" ");
				for(int i = 0; i < word.length; i++){
					if(!wordCount.containsKey(word[i])){
						wordCount.put(word[i], 1);
						
					}
					else{
						
						wordCount.replace(word[i], wordCount.get(word[i]), wordCount.get(word[i])+1);
					}
					
				}
			}

		}
		catch(FileNotFoundException e){
			System.err.println("File Not Found " + filereader);
		}
		catch(IOException e){
			System.err.println("Input error");
		}
		
		
	}
	
	public int hasWord(String word){
		int count = 0;
		if(wordCount.containsKey(word)){
			count = wordCount.get(word);
		}
		return count ;
	}
	
	public String getCountData(){
		String s = "";
		for(String str: wordCount.keySet()){
			s = s  + str + " " + wordCount.get(str) + "\n";
		}
		
		return s;

	}
}
