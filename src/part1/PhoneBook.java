package part1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBook {
	String file ;
	FileReader filereader ;
	
	
	public PhoneBook(String file) throws FileNotFoundException{
		this.file = file;

	}
	
	public void readBook() {
		try{
			System.out.println("-------- Phone Book ---------");
			System.out.println("Name  Phone");
			System.out.println("-----------");
			System.out.println("-----------");
			filereader = new FileReader(this.file);
			BufferedReader buffer = new BufferedReader(filereader);
			
			int sum = 0;
			for (file = buffer.readLine(); file != null; file = buffer.readLine()) {
				System.out.println(file);
				sum += 1;
			}
		} 
		catch(FileNotFoundException e){
			System.err.println("File Not Found");
		}
		catch(IOException e){
			System.err.println("Error");
		}
		finally {
			try {
				if (filereader != null)
					filereader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
}
